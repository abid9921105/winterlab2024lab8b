public enum Tile{
	BLANK("_"),
	WALL("W"), 
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	private final String tilename;
	
	private Tile (String tilename){
		this.tilename = tilename;
	}
	public String getTile(){
		return this.tilename;
	}
}

	
	
	
		
		