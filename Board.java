import java.util.Random;

public class Board{
	private Tile[][] grid;
	private final int size;
	
	
	public Board(){
	this.size = 5;
	this.grid = new Tile[this.size][this.size];
	Random rng = new Random();
	
	for(int i = 0; i < this.grid.length; i++)
		{
		int randIndex = rng.nextInt(this.grid[i].length);
		for(int j = 0; j < this.grid[i].length ; j++)
			{
				//filling all spots on the grid with BLANK and then overriding rng spots with HIDDEN_WALL
			grid[i][j] = Tile.BLANK;
			grid[i][randIndex] = Tile.HIDDEN_WALL;
			}
		}
	}
	public String toString(){
		String builder = "";
		for (int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid[i].length ; j++){
				
				builder += this.grid[i][j].getTile() + " ";
			// builds the grid setup while seperating the arrays with a new line
			}
			builder += "\n";
		}
		return builder;
	}
	public int placeToken(int row, int col){
		if (row >= this.size || col >= this.size || row < 0 || col < 0){
		return -2;
		}
		// if out of bounds return negative number
		
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL ){
		return -1;
		}
		// if its a castle or a wall in that spot -1
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){
			
		this.grid[row][col] = Tile.HIDDEN_WALL;
		return 1; 
		// if hidden wall 1
		}else{
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
		// here its if its blank we update the tile to a castle and return 0
	
	}
}