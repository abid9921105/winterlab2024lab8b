import java.util.Scanner;

public class BoardGameApp{
	public static void main(String[] args){
	 Scanner reader = new Scanner(System.in);
	 System.out.println("Welcome to Abid Castle");
	 
	 int numCastles = 7;
	 int turns = 0;
	 Board board = new Board();
	 
	 while(numCastles > 0 && turns < 8){
		System.out.println(board);
		System.out.println("Number of castles: " + numCastles);
		System.out.println("Number of turns :" + turns);
			
		System.out.println("INPUT TWO INTS REPRESENTING A ROW AND A COLUMN");
		
		int row = reader.nextInt();
		int col = reader.nextInt();
		
	    int placedToken = board.placeToken(row,col);
		
		while(placedToken < 0){
			System.out.println("Re-enter the row and column values!");
			row = reader.nextInt();
			col = reader.nextInt();
			placedToken = board.placeToken(row,col);
		}
		if(placedToken == 1){
		System.out.println("There is a wall at that position");
		turns++;
		}
		else if(placedToken == 0){
		System.out.println("Successfully placed castle tile");
		turns++;
		numCastles--;
		}
		}

	System.out.print(board);
	if(numCastles == 0){
		System.out.println("YOU HAVE WON THE GAME");
	}else{
	System.out.println("YOU LOST!");
		
	
	}
		
	}
}